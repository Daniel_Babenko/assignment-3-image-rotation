#include <stdlib.h>

#include "io_formats.h"
#include "handle_errors.h"

#define OUT_OF_MEMORY_EXIT_CODE 5
#define INVALID_ARGUMENT_EXIT_CODE 1

void handle_read_errors(enum read_status r_status) {
    printf("Ошибка при чтении: ");
    if (r_status == READ_INVALID_SIGNATURE) {
        printf("неверная сигнатура файла!");
    } else if (r_status == READ_INVALID_BITS) {
        printf("ошибка валидации битов!");
    } else if (r_status == READ_INVALID_HEADER) {
        printf("невалидный заголовок!");
    }
}

void handle_write_errors(enum write_status w_status) {
    printf("Ошибка при записи: ");
    if (w_status == WRITE_INVALID_HEADER) {
        printf("невалидный заголовок!");
    } else if (w_status == WRITE_INVALID_BITS) {
        printf("ошибка валидации битов!");
    }
}

void handle_out_of_memory_error(void) {
    printf("Память переполнена !!!");
    exit(OUT_OF_MEMORY_EXIT_CODE);
}

void handle_invalid_argument(uint32_t arg){
    if (arg <= 0){
        printf("Невалидный аргумент: данный аргумент должен быть строго больше 0!");
        exit(INVALID_ARGUMENT_EXIT_CODE);
    }
}
