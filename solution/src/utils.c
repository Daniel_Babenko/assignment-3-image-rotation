#include <malloc.h>
#include <stdlib.h>
#include <string.h>

#include "handle_errors.h"
#include "image.h"
#include "utils.h"

struct image get_image(uint32_t width, uint32_t height) {
    struct image img;
    handle_invalid_argument(width);
    handle_invalid_argument(height);
    img.width = width;
    img.height = height;
    img.data = malloc(width * height * sizeof(struct pixel));

    if (img.data == NULL) {
        handle_out_of_memory_error();
    }

    return img;
}

const char* get_file_extension(const char* filename) {
    const char* dot = strrchr(filename, '.');
    if(!dot || dot == filename) {
        return "";
    }
    return dot + 1;
}

uint32_t get_paddings(uint32_t width) {
    uint32_t row_size = width * sizeof(struct pixel);
    uint32_t padding = 4 - row_size % 4;
    if (padding % 4 == 0) {
        return 0;
    } else {
        return padding;
    }
}
