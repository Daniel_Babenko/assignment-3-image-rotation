//
// Created by danie on 14.11.2023.
//
#include <stddef.h>

#include "handle_errors.h"
#include "image.h"
#include "transform.h"
#include "utils.h"

#define ANGLE_1 90
#define ANGLE_2 180
#define ANGLE_3 270

uint32_t row_transform(uint32_t row, uint32_t column, int angle, struct image const source){
    uint32_t new_row = 0;
    switch(angle){
        case ANGLE_1:
            new_row = source.width - 1 - column;
            break;
        case ANGLE_2:
            new_row = source.height - 1 - row;
            break;
        case ANGLE_3:
            new_row = column;
            break;
    }

    return new_row;
}

uint32_t column_transform(uint32_t row, uint32_t column, int angle, struct image const source){
    uint32_t new_column = 0;
    switch(angle){
        case ANGLE_1:
            new_column = row;
            break;
        case ANGLE_2:
            new_column = source.width - 1 - column;
            break;
        case ANGLE_3:
            new_column = source.height - 1 - row;
            break;
    }

    return new_column;
}

struct image create_image_by_angle(uint32_t width, uint32_t height, int angle){
    struct image new_image;
    if (angle%180 == 0){
       new_image = get_image(width, height);
    } else {
      new_image = get_image(height, width);  
    }
    return new_image;
}

struct image rotate(struct image const source, int angle) {
    if (angle == 0) {return source;}
    else if (angle < 0){
        angle = 360 + angle;
    }
    uint32_t height = source.height;
    uint32_t width = source.width;

    uint32_t source_size = width * height * sizeof(struct pixel);

    struct image transformed = create_image_by_angle(width, height, angle);

    for (uint32_t row = 0; row < height; row++) {
        for (uint32_t column = 0; column < width; column++) {
            uint32_t trans_row = row_transform(row, column, angle, source);
            uint32_t trans_column = column_transform(row, column, angle, source);

            uint32_t new_data_index = trans_row * transformed.width + trans_column;

            if ( source_size > new_data_index){
                transformed.data[new_data_index] = source.data[row * width + column];
                } 
            }
        }

    return transformed;
}
