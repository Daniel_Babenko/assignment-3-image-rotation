//
// Created by danie on 08.01.2024.
//

#include <stdio.h>

#include <stdlib.h>
#include <string.h>

#include "handle_errors.h"
#include "image.h"
#include "io_formats.h"
#include "transform.h"
#include "utils.h"

#define NUMBER_OF_ARGUMENTS 4
#define SOURCE_IMAGE_ARG_NUMBER 1
#define TRANSFORMED_IMAGE_ARG_NUMBER 2
#define ANGLE_ARG_NUMBER 3

#define NO_SUCH_SOURCE_EXIT_CODE 5
#define READ_ERROR_EXIT_CODE 6
#define WRITE_ERROR_EXIT_CODE 7
#define WRONG_FILE_EXTENSION 8

int main(int argc, char **argv) {
    if (argc != NUMBER_OF_ARGUMENTS) {
        printf("Number of args error! \n");
        return 1;
    }

    const char *source_image = argv[SOURCE_IMAGE_ARG_NUMBER];
    const char *transformed_image = argv[TRANSFORMED_IMAGE_ARG_NUMBER];
    const int angle = atoi(argv[ANGLE_ARG_NUMBER]);

    FILE *input_file = fopen(source_image, "rb");
    FILE *output_file = fopen(transformed_image, "wb");       // if this file doesn't exist, it's created

    if (input_file == NULL) {
        printf("Файл, из которого вы пытаетесь считать изображение, не существует :(");
        return NO_SUCH_SOURCE_EXIT_CODE;
    }

    struct image source;

    const char* input_file_extension = get_file_extension(source_image);
    const char* output_file_extension = get_file_extension(transformed_image);
    //int strategy = choose_strategy_by_extensions(input_file_extension, output_file_extension);

    if (strcmp(input_file_extension, "bmp") == 0 || strcmp(input_file_extension, "g24") == 0) {

    enum read_status r_status = from_bmp(input_file, &source);

    if (r_status != READ_OK) {
        
        r_status = from_g24(input_file, &source);
        
        if (r_status != READ_OK) {
            handle_read_errors(r_status);
            return READ_ERROR_EXIT_CODE;
        }
    }
    
    } else {
        printf("Формат файла не поддерживается");
        return WRONG_FILE_EXTENSION;
        }

    struct image transformed;

    if (angle % 90 == 0 && angle < 360 && angle > -360){
        transformed = rotate(source, angle);
    } else {
        printf("Угол поворота задан неверно!!!\n");
        printf("Валидны следующие значения: -270, -180, -90, 0, 90, 180, 270.");
        return 1;
    }

    if (strcmp(output_file_extension, "bmp") == 0 || strcmp(output_file_extension, "g24") == 0) {
        enum write_status w_status = to_bmp(output_file, &transformed);

    if (w_status != WRITE_OK) {

        w_status = to_g24(output_file, &source);
        
        if (w_status != WRITE_OK) {
            handle_write_errors(w_status);
            return WRITE_ERROR_EXIT_CODE;
        }
    }
    
    } else {
        printf("Формат файла не поддерживается");
        return WRONG_FILE_EXTENSION;
    }

    if (source.data != transformed.data) {
        free(transformed.data);
    }
    free(source.data);

    fclose(output_file);
    fclose(input_file);

    printf("Хорошая работа!");
    return 0;
}
