#include "image.h"

#ifndef TRANSFORM_H
#define TRANSFORM_H

struct image rotate(struct image source, int angle);

#endif //TRANSFORM_H
