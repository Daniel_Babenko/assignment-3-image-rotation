#ifndef HANDLE_ERRORS_H
#define HANDLE_ERRORS_H

#include <stdlib.h>

#include "io_formats.h"

void handle_read_errors(enum read_status r_status);

void handle_write_errors(enum write_status w_status);

void handle_out_of_memory_error(void);

void handle_invalid_argument(uint32_t arg);

#endif //HANDLE_ERRORS_H
