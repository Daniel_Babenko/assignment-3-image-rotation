#ifndef UTILS_H
#define UTILS_H

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

struct image get_image(uint32_t width, uint32_t height);

const char* get_file_extension(const char* filename);

uint32_t get_paddings(uint32_t width);

#endif //UTILS_H
