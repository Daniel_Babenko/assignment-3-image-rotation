file(GLOB_RECURSE sources CONFIGURE_DEPENDS
        solution/src/*.c
        solution/src/*.h
        include/*.h
)

add_executable(image-transformer ${sources}
        src/transform.c
        include/io_formats.h
        src/transform.c
        include/transform.h
        src/handle_errors.c
        include/handle_errors.h
        include/utils.h
        src/io_formats.c
        src/handle_errors.c
        src/transform.c
        src/utils.c
        src/main.c
)
target_include_directories(image-transformer PRIVATE src include)

